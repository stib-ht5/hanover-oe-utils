#!/bin/sh

set -eu

cd "$(git rev-parse --show-toplevel)"

PACKAGE="$(git config --list | sed -n -e 's/\.git$//' -e 's|^remote.[^=]*.url=.*/||p' | head -n1)"

tags_sorted() {
	git for-each-ref --format='%(refname)' refs/tags/ | while read refname; do
		refname="${refname#refs/tags/}"
		tag="${refname#${PACKAGE}_}"
		printf "$tag\t$refname\n"
	done | sort -V | cut -f2-
}

LAST_TAG=$(tags_sorted | tail -n1)
NEXT_TAG="${1:-}"
PRODUCT_DIR="${2:-}"
TAGS=$(printf "$LAST_TAG\n$NEXT_TAG" | sort -V)

info() {
	cat <<-EOT
	#
	# $*
	#
	EOT
}

die() {
	echo "E: $*" >&2
	exit 1
}

git_changelog() {
	git log --pretty=format:'%h %ad %an %s' --graph --date=short --color=never "$LAST_TAG.."
}

# verify next > last
#
if [ -n "$NEXT_TAG" -a "$NEXT_TAG" != "$LAST_TAG" -a "$TAGS" = "$LAST_TAG
$NEXT_TAG" ]; then
	info "$NEXT_TAG > $LAST_TAG"
else
	die "$NEXT_TAG <= $LAST_TAG"
fi

git status -s
[ -z "$(git ls-files -md)" ] || die "$PWD: is dirty"

NOW="$(date +"%Y-%m-%d %H:%M:%S %Z")"
CHANGES="$(git_changelog)"
if [ -n "$CHANGES" ]; then
	HAS_CHANGES=true
	CHANGES="$CHANGES
"
else
	HAS_CHANGES=false
fi

F=ChangeLog
if [ -f $F ]; then
	cat <<EOT | tee $F~
=====================================================================
RELEASE $NEXT_TAG
ISSUEBY $USER
DATE	$NOW
=====================================================================

EOT
	if $HAS_CHANGES; then
		echo "$CHANGES"
	else
		echo "* none"
	fi | tee -a $F~
	
	if [ -s $F ]; then
		cat $F >> $F~
	fi
	mv $F~ $F
	git commit -s $F -m "$NEXT_TAG"
fi

git tag -a "$NEXT_TAG" -m "$NEXT_TAG"
if [ -s configure.ac ]; then
	updateconfig.sh
	git tag -d "$NEXT_TAG"
	if [ -s $F ]; then
		git commit --amend --no-edit configure.ac
	else
		git commit -m "$NEXT_TAG" configure.ac
	fi
	git tag -a "$NEXT_TAG" -m "$NEXT_TAG"
fi

if [ -d "$PRODUCT_DIR" ]; then
	PRJ=${PWD##*/}
	PRJ=${PRJ%%-git}
	PRJ_VER=${NEXT_TAG#${PRJ}_}
	F=$PRODUCT_DIR/NOTES.txt

	if [ -s "$F" ]; then
		echo >> $F
	fi

	cat <<EOT >> $F
=====================================================================
$PRJ $PRJ_VER
=====================================================================
EOT
	if $HAS_CHANGES; then
		echo "$CHANGES"
	else
		echo "* none"
	fi >> $F
fi

git push $(git remote | head -n1) HEAD:master "$NEXT_TAG"
