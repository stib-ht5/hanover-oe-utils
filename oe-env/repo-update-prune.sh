#!/bin/sh

# this script is intended to run from the parent directory of your repo
# workspaces so */.repo exists
#
# additionally, if oe-mirror exists and no arguments are provided
# it will synchronize that one first
#
set -eu

export PAGER=

JOBS=8
REPO_PRUNE=false
REPO_OVERVIEW=false

git_prune() {
	local dir="$1" repo="$2"
	cat <<-EOT

	# $dir/$repo
	#
	EOT

	cd "$repo"
	git gc --auto
	git remote update --prune
	if [ -d .git ]; then
		git status -s
	fi
	cd - > /dev/null
}

repo_prune() {
	local oldpwd="$PWD" dir="$1"
	local bare=false
	local repos=

	cat <<-EOT
	#
	# $dir
	#
	EOT

	cd "$dir"
	repo sync -j$JOBS
	git_prune "$dir" ".repo/manifests"

	repos="$(repo list -f | cut -d' ' -f1 | sed -e '/^None$/d;' -e "s|^$PWD/||" || true)"
	if [ -z "$repos" ]; then
		bare=true
	       	repos="$(repo list -n | sed -e 's|$|.git|')"
	fi

	for r in $repos; do
		git_prune "$dir" "$r"
	done

	if ! $bare; then
		if $REPO_PRUNE; then
			repo prune || true
		else
			repo branch
		fi

		if $REPO_OVERVIEW; then
			repo info -o
		fi
	fi

	cd "$oldpwd"
}

while [ $# -gt 0 ]; do
	case "$1" in
	-p)	REPO_PRUNE=true; shift ;;
	-o)	REPO_OVERVIEW=true; shift ;;
	*)	break ;;
	esac
done

if [ $# -gt 0 ]; then
	for r; do
		if [ -s "$r/.repo/manifest.xml" ]; then
			repo_prune "${r%/}"
		else
			echo "# $r - Not a repo workspace"
		fi
	done
elif [ -d oe-mirror/.repo ]; then
	repo_prune oe-mirror
	for r in */.repo/..; do
		if [ ! -d "$r" ]; then
			:
		elif ! expr "$r" : "oe-mirror/" > /dev/null; then
			repo_prune "${r%%/*}"
		fi
	done
else
	for r in ./.repo/.. */.repo/..; do
		if [ -d "$r" ]; then
			repo_prune "${r%%/*}"
		fi
	done
fi
