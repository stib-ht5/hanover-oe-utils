#!/bin/sh

set -eu

cd "$(dirname "$0")"

TIMES=times.txt

# PRODUCT= and MACHINE= might come from the env
BB_FILES=
BB_CMD=
BB_RELEASE=
BB_FREEZE=
BB_DEBUG=
BB_FORCE=

RETRY_MAX=4
RETRY=abort

bb_usage() {
	cat <<EOT
bitbake/docker wrapper to streamline the system build process

Usage: $0 [options...] [packages...]

Options:
  -p PRODUCT, --product=PRODUCT
		Specifies application context of this board. if not given
		we will try to infer it from the name of the workspace
  -m MACHINE, --machine=MACHINE
		Name of the machine for which we are building. if not given
		we will try to infer it from the name of the workspace
  -r, --release
		Ignore -dev/-git repositories
  -f, --freeze
		Annotate build as an official release, and ignore -dev/-git
		repositories
  -D, --debug
		Run build in absurdly verbose mode

Multipackage options:
  -k, --continue
		Don't abort if a target fails
  -l, --retry
		Retry the same target if it fails

Bitbake options:
  -b BUILDFILE, --buildfile=BUILDFILE
		Instead of resolving a package from the target name, use the
		given .bb file
  -c CMD, --cmd=CMD
		Specify task to execute
  -F, --force
		Force run of specificed cmd, regardless of the stamp status

Supported values for PRODUCT:
EOT
	ls -1 hanover-products/*/conf/layer.conf hanover-products/*/release.inc |
		cut -d/ -f2 | sort -Vu | sed -e 's|^|  * |'
	cat <<EOT

Supported values for MACHINE:
EOT
	ls -1 hanover-system/conf/machine/*.conf |
		sed -e 's|.*/\([^/.]\+\)\.conf|  * \1|' | sort -Vu

}

longopts="product:,machine:,buildfile:,cmd:,release,freeze,debug,force"
shortopt="p:m:b:c:rfDF"

longopts="$longopts,continue,retry"
shortopt="${shortopt}kl"

# preprocess arguments list
if ! options=$(getopt -n bb -o "$shortopt" -l "$longopts" -- "$@"); then
	bb_usage
	exit 1
fi

# load new arguments list
eval set -- "$options"

while [ $# -gt 0 ]; do
	case "$1" in
	-p|--product)	PRODUCT="$2"; shift ;;
	-m|--machine)	MACHINE="$2"; shift ;;
	-b|--buildfile)	BB_FILES="${BB_FILES:+$BB_FILES }$2"; shift ;;
	-c|--cmd)	BB_CMD="$2"; shift ;;

	-r|--release)	BB_RELEASE=true ;;
	-f|--freeze)	BB_FREEZE=true ;;
	-D|--debug)	BB_DEBUG=true ;;
	-F|--force)	BB_FORCE=true ;;

	-k|--continue)	RETRY=skip ;;
	-l|--retry)	RETRY=retry ;;

	--)	shift; break ;;
	esac
	shift
done

# attempt to detect PRODUCT
if [ -z "${PRODUCT:-}" ]; then
	PRODUCT=

	case "${PWD##*/}" in
	*tis*)	PRODUCT=tis ;;
	*stib*)	PRODUCT=stib ;;
	*tft*)	PRODUCT=iptft ;;
	*classic*|*htc*)
		PRODUCT=classic ;;
	*)
		PRODUCT= ;;
	esac
fi

# attempt to detect MACHINE
if [ -z "${MACHINE:-}" ]; then
	case "${PWD##*/}" in
	*ht5*)	MACHINE=dm814x-ht5 ;;
	*ht2*|*ht3*|*htc*)
		MACHINE=dm365-htc ;;
	*z3*)
		MACHINE=dm814x-z3 ;;
	*)
		echo "MACHINE not inferred from dirname (${PWD##*/})" >&2
		exit 1
		;;
	esac
fi

bb_run() {
	local ts= fmt=

	set -- "bb" \
		-m "$MACHINE" \
		${PRODUCT:+-p "$PRODUCT"} \
		${BB_CMD:+-c "$BB_CMD"} \
		${BB_RELEASE:+-r} \
		${BB_FREEZE:+-f} \
		${BB_DEBUG:+-D} \
		${BB_FORCE:+-F} \
		"$@"

	ts="$(date '+%Y-%m-%dT%H:%M:%S')"
	echo "# $* (start:$ts)" >> $TIMES

	# escape potential %s
	fmt="$(echo "$*" | sed -e 's/%/%%/g')"
	fmt="%e # $fmt (start:$ts, exit:%x)"

	# remove 'bb'
	shift

	if [ -x ./run.sh ]; then
		set -- ./run.sh "$@"
	else
		set -- arago-docker-run "$@"
	fi

	# and run
	time -q -o $TIMES -a -f "$fmt" "$@"
}

bb_run_all() {
	local go=true
	local x= err=0

	trap 'go=false' INT TERM

	# buildfiles
	for x in $BB_FILES; do
		if $go; then
			bb_run -b "$x" || err=$?
			if [ "$RETRY" = abort -a $err -ne 0 ]; then
				break
			fi
		else
			[ $err -ne 0 ] || err=130
			break
		fi
	done

	# targets
	for x; do
		if $go; then
			bb_run "$x" || err=$?
			if [ "$RETRY" = abort -a $err -ne 0 ]; then
				break
			fi
		else
			[ $err -ne 0 ] || err=130
			break
		fi
	done

	exit $err
}

bb_retry_run_one() {
	local retry=$RETRY_MAX
	local err=130

	while $go; do
		err=0
		bb_run "$@" || err=$?
		if [ $err -eq 0 -o $retry -eq 0 ]; then
			break
		else
			retry=$(($retry - 1))
		fi
	done

	return $err
}

bb_retry_run_all() {
	local go=true err=0
	local x=

	trap 'go=false' INT TERM

	# buildfiles
	for x in $BB_FILES; do
		bb_retry_run_one -b "$x" || err=$?
		[ $err -eq 0 ] || break
	done

	# targets
	for x; do
		bb_retry_run_one "$x" || err=$?
		[ $err -eq 0 ] || break
	done

	exit $err
}

if [ -z "$BB_FILES" -a $# -eq 0 ]; then
	set -- hanover-${PRODUCT:-base}-image
fi

if [ "$RETRY" = retry ]; then
	bb_retry_run_all "$@"
else
	bb_run_all "$@"
fi
