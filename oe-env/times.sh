#!/bin/sh

set -eu

TIMES_TXT="${1:-$(dirname "$0")/times.txt}"
[ -s "$TIMES_TXT" ] || exit 0

if [ "$TIMES_TXT" = ./times.txt ]; then
	# avoid printing a useless title
	TIMES_TXT="$(readlink -f "$TIMES_TXT")"
fi

cat <<EOT
# ${TIMES_TXT#$HOME/}

EOT

# sum time of all previous runs
total=0
while read l; do
	case "$l" in
	"Command terminated by signal"*)
		continue
		;;
	*"#"*)
		extra="$(echo "${l#*#}" | sed -e 's/^[ \t]*//' -e 's/[ \t]\+$//')"
		;;
	*)
		extra=
		;;
	esac
	l=$(echo "${l%%#*}" | sed -e 's/^[ \t]\*//' -e 's/[ \t]\+$//')
	if [ -n "$l" ]; then
		printf "+ %8.2fs${extra:+ # $extra}\n" "$l"
		total=$(dc -e "2k $total $l + p")
	fi
done < "$TIMES_TXT"

# and dynamically add the running time of the current, if anything is still running.
extra="$(tail -n1 "$TIMES_TXT" | sed -n -e 's/[ \t]\+$//' -e 's/^#[ \t]*\(.*start:.*\)/\1/p')"
start=$(echo "$extra" | sed -e 's/.*start:\(.*\))$/\1/')
if [ -n "$start" ]; then
	t1=$(date +"%s.%N")
	t2=$(date -d "$start" +"%s.%N")
	l=$(dc -e "2k $t1 $t2 - p")
	printf "+ %8.2fs${extra:+ # $extra}\n" "$l"
	total=$(dc -e "2k $total $l + p")
fi
echo "==========="

m=$(dc -e "0k $total 60 / p")
s=$(dc -e "2k $total $m 60 * - p")
h=$(dc -e "0k $m 60 / p")
m=$(dc -e "0k $m $h 60 * - p")

printf "%10.2fs (%02u:%02u:%05.2f)\n\n" "$total" "$h" "$m" "$s"
