#!/bin/sh

set -eu

FIND_WORK_TREE_DEPTH=6
DEBUG=false
BASELINE=false

DEFAULT_REMOTE=git
DEFAULT_REVISION=master

REMOTE_EBS_FETCH="ssh://ebs/opt/git"
REMOTE_GIT_FETCH=..
REMOTE_GITHUB_FETCH=https://github.com
REMOTE_GOLANG_FETCH=https://golang.org/x
REMOTE_YOCTO_FETCH=https://git.yoctoproject.org/git
REMOTE_OE_FETCH=$REMOTE_GITHUB_FETCH/openembedded
REMOTE_FSL_FETCH=$REMOTE_GITHUB_FETCH/freescale
REMOTE_BB_FETCH=https://bitbucket.org
REMOTE_KO_FETCH=https://git.kernel.org/pub/scm/linux/kernel/git

TAB="$(printf '\t')"

# logging
#
PREFIX= DEBUG_PREFIX=

log() {
	if [ $# -eq 0 ]; then
		cat
	else
		echo "$*"
	fi | sed -e "s|^|# ${PREFIX:-}|" -e 's| *$||' >&2
}

debug() {
	! $DEBUG || PREFIX="${DEBUG_PREFIX:-}" log "$@"
}

warn() {
	PREFIX="W: " log "$@"
}

err() {
	PREFIX="E: " log "$@"
}

die() {
	PREFIX="FATAL: " log "$@"
	exit 1
}

# command line arguments
#
usage() {
	cat <<-EOT >&2
	Usage: $0 [--options]

	options:
	-d, --debug	Show debugging info
	-b, --baseline	Use tags instead of branches
	-R, --remote    Select default remote (ebs*, git)
	EOT
	exit 1
}

shortopts="dbR:B:?"
longopts="debug,baseline,remote,revision,help"
if ! options=$(getopt -o "$shortopts" -l "$longopts" -- "$@"); then
	usage
fi

eval "set -- $options"

while [ $# -gt 0 ]; do
	case "$1" in
	-d|--debug)	DEBUG=true ;;
	-b|--baseline)	BASELINE=true ;;
	-R|--remote)	DEFAULT_REMOTE="$2"; shift ;;
	-B|--revision)	DEFAULT_REVISION="$2"; shift ;;
	--)		shift; break ;;
	*) # -?|--help)
		usage
	esac
	shift
done

# git helpers
#
GIT=`which git`

git() {
	local worktree="$DIR" gitdir="$DIR/.git"

	while [ ! -d "$gitdir" ]; do
		if [ -L "$gitdir" ]; then
			gitdir="$(readlink -f "$gitdir" )"
		fi
		if [ -f "$gitdir" ]; then
			read gitdir < "$gitdir"
		fi
	done

	set -- "$GIT" --work-tree="$worktree" --git-dir="$gitdir" "$@"
	debug "git: $*"
	"$@"
}

git_get_rev() {
	git rev-parse "$1"
}

git_pretty_d_to_refs() {
	local r= l= x=

	sed -n -e 's|,||g' -e 's|tag: |tag:|g' -e 's|.* (\([^)]\+\))|\1|p' | while read l; do
		for r in $l; do
			case "$r" in
			HEAD|*/HEAD|"->")
				continue
				;;
			tag:*)
				x="refs/tags/${r#tag:}"
				if git show-ref -q --verify $x; then
					echo "$x"
					continue
				fi
				;;
			*)
				for x in refs/heads refs/tags refs/remotes; do
					if git show-ref -q --verify "$x/$r"; then
						echo "$x/$r"
						continue 2
					fi
				done
			esac

			err "$DIR: $r: invalid reference"
		done | sort -V | tr '\n' ' '
	done
}

git_pretty_d_to_remote_refs() {
	local r= l= x=

	stdbuf -o0 sed -n -e 's|,||g' -e 's|tag: |tag:|g' -e 's|.* (\([^)]\+\))|\1|p' | while read l; do
		for r in $l; do
			case "$r" in
			HEAD|*/HEAD|tag:*|"->")
				continue
				;;
			*/*)
				x="refs/remotes/$r"
				if git show-ref -q --verify "$x"; then
					if [ -n "$(git config "remote.${r%%/*}.url")" ]; then
						echo "$x"
					fi

					continue
				fi
				;;
			esac
		done | sort -V | tr '\n' ' '
		echo
	done | stdbuf -o0 sed -e '/^$/d;'
}

git_get_refs() {
	git show -s --pretty="%d" "$1" | git_pretty_d_to_refs
}

# file system scanning
#
find_work_tree() {
	local count="$1" base="$2"
	local x= d=
	shift 2

	if [ $count -eq 0 ]; then
		return
	else
		count=$((count - 1))
	fi

	[ ! -e "$base/.git" ] || echo "${base#./}"

	for d in "$base"/*; do
		if [ -d "$d" ]; then
			find_work_tree $count "$d" &
		fi
	done
	wait
}

# manifest.xml rendering
#
indent() {
	sed -e "s|^|$TAB|" -e "s|[$TAB ]\+$||"
}

xml_comment() {
	printf "<!-- "
	if [ $# -eq 0 ]; then
		cat
	else
		echo -n "$*"
	fi | sed -e 's|-|\xe2\x80\x93|g'
	# -- is forbidden within comments, but because of
	# patch readability we prefer utf8("\u2013") instead of &#45;
	printf " -->\n"
}

render_manifest_xml_project() {
	local path="$1" name="$2" remote="$3" revision="$4" comment="$5"
	local q='"' x=
	local empty=

	shift 5

	[ "$revision" != "$DEFAULT_REVISION" ] || revision=
	[ "$remote" != "$DEFAULT_REMOTE" ] || remote=

	if [ -z "$comment" -a $# -eq 0 ]; then
		empty=true
		x=" />"
	else
		empty=false
		x=">"
	fi

	cat <<-EOT
	<project path="$path"${remote:+ remote=$q$remote$q} name="$name"${revision:+ revision=$q$revision$q}$x
	EOT

	for x; do
		echo "<linkfile dest=\"${x%:*}\"${TAB}src=\"${x#*:}\" />"
	done | sort | column -s"$TAB" -c2 -t | indent

	if [ -n "$comment" ]; then
		echo "$comment" | xml_comment | indent
	fi

	$empty || echo "</project>"
}

# remote url
split_remote_url() {
	local var_remote="$1" var_name="$2" remote="$3" name=
	local url= x=

	url="$(git config "remote.$remote.url" || true)"
	case "$url" in
	*@*)   x="${url#*@}" ;;
	*://*) x="${url#*://}" ;;
	*:*)   x="$url" ;;
	esac

	if [ -n "$x" ]; then
		remote="${x%%/*}"
		remote="${remote%%:*}"
		case "$x" in
		$remote/*)  name="${x#$remote/}" ;;
		$remote:/*) name="${x#$remote:/}" ;;
		$remote:*)  name="${x#$remote:}" ;;
		esac
	fi

	case "$remote" in
	bitbucket.org)
		remote=bb
		;;
	go.googlesource.com)
		remote=golang
		;;
	github.com)
		case "$name" in
		freescale/*|Freescale/*)
			remote=freescale
			name=${name#*/}
			;;
		openembedded/*)
			remote=oe
			name=${name#*/}
			;;
		*)
			remote=github
			;;
		esac
		;;
	git.yoctoproject.org)
		remote=yocto
		name="${name#git/}"
		;;
	git.openembedded.org)
		remote=oe
		name="${name#git/}"
		;;
	git.kernel.org)
		remote=ko
		name=${name#pub/scm/linux/kernel/git/}
		;;
	git|git.hanover.local) remote=git ;;
	ebs|ebs.hanover.local|192.168.0.56)
		remote=ebs
		name="${name#opt/git/}"
		name="${name#/opt/git/}"
		;;
	*)
		err "remote.$remote.url: not recognized ($url -> $x)"
		remote=
		name=
	esac

	echo "$var_remote=$remote $var_name=${name%.git}"
}

choose_remote() {
	local remote_var="$1" name_var="$2" valid_var="$3"
	local remote= name= valid=
	local x= y= z=

	shift 3

	for x; do
		if echo "$valid" | grep -q "^$x\$"; then
			# known remote, skip
			continue
		fi

		# y=remote z=name
		eval "$(split_remote_url y z "$x")"

		if [ -n "$y" -a -n "$z" ]; then
			if [ -z "$remote" ]; then
				# first remote
				remote="$y"
				name="$z"
				valid="$x"
			elif [ "$y" = "$DEFAULT_REMOTE" ]; then
				# preferred
				err "$DIR: remote: $x/$y replacing $remote"
				remote="$y"
				name="$z"
				valid="$x${valid:+
$valid}"
			else
				warn "$DIR: remote: $x/$y ignored because we prefer $remote"
				valid="${valid:+$valid
}$x"
			fi
		fi
	done
	valid="$(echo "$valid" | tr '\n' ' ' | sed -e 's/^ *//' -e 's/ *$//')"

	echo "$remote_var=$remote $name_var="$name" $valid_var='$valid'"
}

find_best_remote() {
	local remote_var="$1" name_var="$2" valid_var="$3"
	local l= remote= name= valid=

	git log --pretty="%d" "HEAD" | git_pretty_d_to_remote_refs | while read l; do
		eval "$(choose_remote remote name valid $(echo "$l" | tr ' ' '\n' | cut -d/ -f3))"
		if [ -n "$valid" ]; then
			echo "$remote_var=$remote $name_var=$name $valid_var='$valid'"
			break
		fi
	done
}

tags_sorted() {
	git for-each-ref --format='%(taggerdate:raw) %(committerdate:raw) %(refname)' "$@" |
		while read td ttz cd ctz refname; do
			[ -n "$ctz" ] || refname="$cd"

			echo "$td $refname"
	done | sort -V | cut -d'/' -f3-
}


find_best_revision() {
	local revision_var="$1" branches_var="$2" tags_var="$3"
	local revision= remotes= tags= branches= heads=
	shift 3

	HEAD_REFS="$(git_get_refs "HEAD")"

	# remotes in multi-line (without calling tr)
	for x; do
		remotes="${remotes:+$remotes
}$x"
	done

	for x in $HEAD_REFS; do
		case "$x" in
		refs/tags/*)
			tags="${tags:+$tags }$x"
			;;
		refs/heads/*)
			heads="${heads:+$heads }${x#refs/heads/}"
			;;
		refs/remotes/*)
			x="${x#refs/remotes/}" # branch
			y="${x%%/*}" # remote

			# unknown remote?
			if ! echo "$remotes" | grep -q "^$y$"; then
				continue
			fi

			branches="${branches:+$branches }$x"
			if [ -z "$revision" -o "$x" = "$y/$DEFAULT_REVISION" ]; then
				# catch default or first
				revision="${x#$y/}"
			fi
			;;
		*)
			err "$DIR: $x: unhandled reference"
			;;
		esac
	done

	if [ -n "$tags" ]; then
		tags="$(tags_sorted $tags | tac | tr '\n' ' ')"
	fi

	echo "$revision_var=$revision $branches_var='$branches' $tags_var='$tags'"
}

find_symlinks() {
	local l= d= x=

	for l in $(find . -maxdepth 1 -type l | sort); do
		x="$(readlink "$l")"
		for d; do
			if [ -e "$x" -a "$x" != "${x#$d/}" ]; then
				echo "${l#./}\t$d\t${x#$d/}"
			fi
		done | sort | head -n1
	done
}

process_work_tree() {
	local DIR="$1" name= revision= remote=
	local HEAD= DIRT=
	local REMOTES= TAGS= BRANCHES=
	local x= y= z=
	shift

	debug "DIR:   $DIR"
	# status
	#
	HEAD="$(git_get_rev "HEAD")"
	DIRT="$(git status -s)"

	if [ -n "$DIRT" ]; then
		x=$(echo "$DIRT" | wc -l)
		debug "HEAD: $HEAD-dirty ($x changes)"
		DIRT="$x uncommited changes
$DIRT"
	else
		debug "HEAD: $HEAD"
	fi

	# find the best remote
	#
	eval "$(find_best_remote remote name REMOTES)"

	debug "remote:$remote name:$name remotes:'$REMOTES'"

	# find the best branch
	#
	eval "$(find_best_revision revision BRANCHES TAGS $REMOTES)"
	debug <<-EOT
	revision: $revision
	branches: $BRANCHES
	tags: $TAGS
	EOT

	x="$(git diff -p --stat HEAD --)"
	if [ -z "$x" ]; then
		:
	elif [ -n "$DIRT" ]; then
		DIRT="$DIRT

$x"
	else
		DIRT="$x"
	fi

	if $BASELINE; then
		if [ -n "$TAGS" ]; then
			revision="refs/tags/${TAGS%% *}"
		else
			revision=$HEAD
		fi
	elif [ -z "$revision" ]; then
		err "REVISION: no revision found."
	fi

	render_manifest_xml_project "$DIR" "$name" "$remote" "$revision" "$DIRT" "$@" | indent
}

log <<EOT
Generated from $PWD
start: $(date)

EOT

render_header() {
	local q='"' tab="$(printf '\t')"
	cat <<-EOT
	<?xml version="1.0" encoding="UTF-8"?>
	<!-- vim: set ft=xml noet ts=4 sw=4 -->
	<manifest>
	$tab<remote name="ebs"       fetch="$REMOTE_EBS_FETCH" />
	$tab<remote name="git"       fetch="$REMOTE_GIT_FETCH" />
	$tab<remote name="github"    fetch="$REMOTE_GITHUB_FETCH" />
	$tab<remote name="golang"    fetch="$REMOTE_GOLANG_FETCH" />
	$tab<remote name="bb"        fetch="$REMOTE_BB_FETCH" />
	$tab<remote name="yocto"     fetch="$REMOTE_YOCTO_FETCH" />
	$tab<remote name="oe"        fetch="$REMOTE_OE_FETCH" />
	$tab<remote name="freescale" fetch="$REMOTE_FSL_FETCH" />
	$tab<remote name="ko"        fetch="$REMOTE_KO_FETCH" />

	$tab<default ${DEFAULT_REMOTE:+remote=$q$DEFAULT_REMOTE$q }revision="$DEFAULT_REVISION" />

	EOT
}

render_footer() {
	cat <<-EOT
	</manifest>
	EOT
}

render_header
set -- $(find_work_tree $FIND_WORK_TREE_DEPTH . | grep -v -e ^downloads/ -e ^build/ -e '^build-*' | sort -V)

SYMLINKS="$(find_symlinks "$@")"

for x; do
	process_work_tree "$x" $(echo "$SYMLINKS" | sed -n "s|\(.*\)$TAB$x$TAB\(.*\)|\1:\2|p")
done
render_footer
log <<EOT
end: $(date)
EOT
