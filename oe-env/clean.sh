#!/bin/sh

set -eu

cd "$(dirname "$0")"

DELETE_ME="$PWD/delete-me"

run() {
	echo "+ $*" >&2
	"$@"
}

delete() {
	local x=

	for x; do
		if [ "$x" != '.' -a "$x" != '..' -a ! -L "$x" -a -e "$x" ]; then
			echo "# $x (removing)"
			if [ -e $DELETE_ME/$x ]; then
				i=0
				while [ -e "$DELETE_ME/$x.$i" ]; do
					# no `let` on dash
					i=$(expr $i + 1)
				done
				run mv $x "$DELETE_ME/$x.$i"
			else
				if [ ! -d "$DELETE_ME/" ]; then
					[ ! -e "$DELETE_ME" ] || rm -f "$DELETE_ME"
					mkdir -p "$DELETE_ME"
				fi
				run mv $x "$DELETE_ME"
			fi
		fi
	done
}

cat <<EOT
# cleaning workspace at $PWD
#
EOT

delete arago-tmp times.txt

export PAGER=
repo list | sed -n 's|^\(.*-git\) : .*|\1|p' | while read r; do
	echo "# $r (deep cleaning)"
	cd "$r"
	git clean -dfx 
	cd - > /dev/null
done

repo status
repo info -o
